# README #

## What is this repository for? ##

Pulp is a demonstration of WebSocket technology over multiple languages, with a simple chat and a simple user management (IRC Like).

It originally developed to run on GlassFish Javaee Server with a preconfigured Intellij file.

The WebServer project should be link with a J2E server and will run a Javascript demonstration of the project.

The WebClient project is a simple client interface written in Java. WebSocket library is include in the project.

A C++/Qt and a Python are on the road.

An Object-C and CSharp project are in mind, but it's not a priority.

## How do I get set up? ##

### GLASSFISH-SERVER Intellij configuration ###
* YOU CAN SKIP THIS STEP IF KNOW HOW TO DEPLOY AN ARTIFACT
* Add artifacts on project settings
* Install glassfish
* Add a glassfish-local configuration with a domain

## Contribution guidelines ##

Contributions are not welcome for the moment. I need it to share my own experience.

### Server ###
* Support private message
* Support registred user (Maybe with profile picture)
* Make a REST-like system to broadcast user list

### Java ###
* Make a trayicon notification

### C++/Qt ###
* Copy the features of Java version

### Long-time ###
* Maybe use Juce interface and others C++ gui
* Objective-C implementations
* Python implementations

## Who do I talk to? ##

* Repo owner or admin